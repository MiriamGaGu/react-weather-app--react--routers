import React, { Component} from "react";

import Titles from "./components/Titles";
import Form from "./components/Form";
import Weather from "./components/Weather";

// https://www.apixu.com/doc/
const API_KEY = "fa02b4b9c7ac4157a5d32627182012";

class App extends Component {
  state = {
    temperature: undefined,
    city: undefined,
    country: undefined,
    humidity: undefined,
    description: undefined,
    error: undefined
  }

  componentDidMount = () => {
    getWeather = (e) =>{
      e.preventDefault();
    const city = e.target.elements.city.value;
    const country = e.target.elements.country.value;
    fetch(`http://api.apixu.com/v1/forecast.json?key=${API_KEY}&q=07112&days=7`)
      .then(res => res.json())
      .then(data => {
        if (city && country) {
          this.setState({
            temperature: data.main.temp,
            city: data.name,
            country: data.sys.country,
            humidity: data.main.humidity,
            description: data.weather[0].description,
            error: ""
          });
        } else {
          this.setState({
            temperature: undefined,
            city: undefined,
            country: undefined,
            humidity: undefined,
            description: undefined,
            error: "Please enter the values."
          });
        }
      })
    }
    
  }

  render() {
    return (
      <div>
        <div className="wrapper">
          <div className="main">
            <div className="container">
              <div className="row">
                <div className="col-xs-5 title-container">
                  <Titles />
                </div>
                <div className="col-xs-7 form-container">
                  <Form getWeather={this.getWeather} />
                  <Weather 
                    temperature={this.state.temperature} 
                    humidity={this.state.humidity}
                    city={this.state.city}
                    country={this.state.country}
                    description={this.state.description}
                    error={this.state.error}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
};

export default App;




















 // getWeather = async (e) => {
  //   e.preventDefault();
  //   const city = e.target.elements.city.value;
  //   const country = e.target.elements.country.value;
  //   const api_call = await fetch(`http://api.apixu.com/v1/forecast.json?key=${API_KEY}&q=07112&days=7`);
  //   const data = await api_call.json();
  //   console.log(data)
  //   if (city && country) {
  //     this.setState({
  //       temperature: data.main.temp,
  //       city: data.name,
  //       country: data.sys.country,
  //       humidity: data.main.humidity,
  //       description: data.weather[0].description,
  //       error: ""
  //     });
  //   } else {
  //     this.setState({
  //       temperature: undefined,
  //       city: undefined,
  //       country: undefined,
  //       humidity: undefined,
  //       description: undefined,
  //       error: "Please enter the values."
  //     });
  //   }
  // }